---
####################### Banner #########################
banner:
  title : "Hi, there <br> I'm home alone...<br> if you know what I mean."
  image : "images/banner-art.svg"
  content : "If life is like soup, I'm a fork."
  button:
    enable : true
    label : "Contact With Us"
    link : "contact"

##################### Feature ##########################
feature:
  enable : true
  title : "Something You Need To Know"
  feature_item:
    # feature item loop
    - name : "Clean Code"
      icon : "ti-check"
      content : "With understandability comes readability, changeability and maintainability"
      
    # feature item loop
    - name : "Friday"
      icon : "ti-heart"
      content : "Ai love you"
      
    # feature item loop
    - name : "Monday"
      icon : "ti-heart"
      content : "Ai love you"
      
    # feature item loop
    - name : "Saturday"
      icon : "ti-heart"
      content : "Ai love you"
      
    # feature item loop
    - name : "Wednesday"
      icon : "ti-heart"
      content : "Ai love you"
      
    # feature item loop
    - name : "Sunday"
      icon : "ti-heart"
      content : "Ai love you"
      


######################### Service #####################
service:
  enable : true
  service_item:
    # service item loop
    - title : "It is the most advanced digital marketing and it company."
      images:
      - "images/service-1.png"
      - "images/service-2.png"
      - "images/service-3.png"
      - "images/service-4.png"
      content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat. consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
    # service item loop
    - title : "It is a privately owned Information and cyber security company"
      images:
      - "images/service-1.png"
      content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat. consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
    # service item loop
    - title : "It’s a team of experienced and skilled people with distributions"
      images:
      - "images/service-2.png"
      content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat. consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
    # service item loop
    - title : "A company standing different from others"
      images:
      - "images/service-3.png"
      content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat. consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur. Leo facilisi nunc viverra tellus. Ac laoreet sit vel consquat."
      button:
        enable : true
        label : "Check it out"
        link : "#"
        
################### Screenshot ########################
screenshot:
  enable : true
  title : "Experience the best <br> workflow with us"
  image : "images/screenshot.svg"

  

##################### Call to action #####################
call_to_action:
  enable : true
  title : "Ready to get started?"
  image : "images/cta.svg"
  content : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Consequat tristique eget amet, tempus eu at consecttur."
  button:
    enable : true
    label : "Contact Us"
    link : "contact"
---